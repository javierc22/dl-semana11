Rails.application.routes.draw do
  get 'pages/index'
  get 'pages/about'
  root 'pages#index' # root: Definir como página principal

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
