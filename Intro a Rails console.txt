Running via Spring preloader in process 29637
Loading development environment (Rails 5.1.6)
2.4.1 :001 > Post
 => Post (call 'Post.connection' to establish a connection) 
2.4.1 :002 > Post.connection
 => #<ActiveRecord::ConnectionAdapters::SQLite3Adapter:0x00000004deba80 @transaction_manager=#<ActiveRecord::ConnectionAdapters::TransactionManager:0x00000004d80668 @stack=[],...
2.4.1 :003 > Post
 => Post(id: integer, content: string, created_at: datetime, updated_at: datetime) 
2.4.1 :004 > Post.all
  Post Load (0.3ms)  SELECT  "posts".* FROM "posts" LIMIT ?  [["LIMIT", 11]]
 => #<ActiveRecord::Relation [#<Post id: 1, content: "Hola :)", created_at: "2018-05-09 01:29:07", updated_at: "2018-05-09 01:29:07">]>


2.4.1 :005 > p = Post.new
 => #<Post id: nil, content: nil, created_at: nil, updated_at: nil> 
2.4.1 :006 > p
 => #<Post id: nil, content: nil, created_at: nil, updated_at: nil> 
2.4.1 :007 > p.content = "Hola, soy un post :v"
 => "Hola, soy un post :v" 
2.4.1 :008 > p
 => #<Post id: nil, content: "Hola, soy un post :v", created_at: nil, updated_at: nil> 
2.4.1 :009 > p.save
   (0.1ms)  begin transaction
  SQL (0.9ms)  INSERT INTO "posts" ("content", "created_at", "updated_at") VALUES (?, ?, ?)  [["content", "Hola, soy un post :v"], ["created_at", "2018-05-09 01:38:17.369968"], ["updated_at", "2018-05-09 01:38:17.369968"]]
   (53.3ms)  commit transaction
 => true 
2.4.1 :010 > p
 => #<Post id: 2, content: "Hola, soy un post :v", created_at: "2018-05-09 01:38:17", updated_at: "2018-05-09 01:38:17">


2.4.1 :011 > Post.first
  Post Load (0.3ms)  SELECT  "posts".* FROM "posts" ORDER BY "posts"."id" ASC LIMIT ?  [["LIMIT", 1]]
 => #<Post id: 1, content: "Hola :)", created_at: "2018-05-09 01:29:07", updated_at: "2018-05-09 01:29:07"> 
2.4.1 :012 > Post.last
  Post Load (0.3ms)  SELECT  "posts".* FROM "posts" ORDER BY "posts"."id" DESC LIMIT ?  [["LIMIT", 1]]
 => #<Post id: 2, content: "Hola, soy un post :v", created_at: "2018-05-09 01:38:17", updated_at: "2018-05-09 01:38:17"> 
2.4.1 :013 > Post.count
   (0.2ms)  SELECT COUNT(*) FROM "posts"
 => 2 
2.4.1 :014 > Post.where(id: 2)
  Post Load (0.3ms)  SELECT  "posts".* FROM "posts" WHERE "posts"."id" = ? LIMIT ?  [["id", 2], ["LIMIT", 11]]
 => #<ActiveRecord::Relation [#<Post id: 2, content: "Hola, soy un post :v", created_at: "2018-05-09 01:38:17", updated_at: "2018-05-09 01:38:17">]>
