## Desafío Latam - Semana 11

#### Rails VC

1. Introducción a Rails
2. Levantando el server
3. Rails generate
4. Modificando una vista
5. MVC
6. Rails routes
7. erb
8. link_to
9. Manifiestos

#### Rails MVC (Modelo Vista Controlador)

1. Scaffold
2. Introducción a Rails Console y Modelo
3. Rutas REST e Index
4. Rutas REST Show
5. Rutas REST New y Create
6. Cargando CDN Bootstrap
7. Bootstrap al Index
8. Buscador
9. Buscador con Bootstrap
10. Strong Params
11. Migración manual
12. Gemfile
13. Versiones y entornos
14. Formas de subir una aplicación
15. Revisando las claves SSH
16. Creando una cuenta en Heroku
17. Subiendo a Heroku
